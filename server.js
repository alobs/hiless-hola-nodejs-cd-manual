var express = require('express')
var app = express()   // El siguiente codigo muesntra "Hola mundo" cuando entramos a  http://localhost:3000

app.get('/', function (req, res) {
	res.send('<h1>Hola mundo</h1>')
})

var port = process.env.PORT || '3000';

app.listen(port, function () {
	 console.log('App escuchando en el puerto 3000!')
})

module.exports = app;
